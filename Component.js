sap.ui.define([
	"sap/ui/core/UIComponent",
    'sap/ui/model/odata/v2/ODataModel'
], function (UIComponent, ODataModel) {
	"use strict";

	return UIComponent.extend("andrei.kuzmich.Component", {
		metadata: {
			manifest: "json"
		},
		init : function () {
             /**
             * call the init function of the parent
             */
			UIComponent.prototype.init.apply(this, arguments);

             /**
             * Initialize router
             */
			this.getRouter().initialize();

            /**
             * Create new Model with data from server
             */
            var oModel = new ODataModel("http://localhost:3000/odata", {
                useBatch: false,
                defaultBindingMode: "TwoWay"
            });

            /**
             * set in model order data from server, global
             */
            this.setModel(oModel, "odata");

        },
	});
});