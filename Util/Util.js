sap.ui.define([
    "sap/ui/core/routing/History"
], function(History) {
    "use strict";

    return {
        /**
         * Makes the transition to the previous page
         */
        onNavBack: function () {
            var oHistory = History.getInstance();
            var sPreviousHash = oHistory.getPreviousHash();

            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                var oRouter = sap.ui.core.UIComponent
                    .getRouterFor(this);
                oRouter.navTo("overview", true);
            }
        }
    };
});