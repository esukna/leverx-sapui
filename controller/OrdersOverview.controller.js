sap.ui.define([
	"sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/ui/model/Filter",
    "sap/ui/model/FilterOperator",
    "sap/m/MessageToast",
    "sap/m/MessageBox"
], function (Controller, JSONModel, Filter, FilterOperator, MessageToast, MessageBox) {
	"use strict";

	return Controller.extend("andrei.kuzmich.controller.OrdersOverview", {
        /**
         * Controller's "init" lifecycle method. Initialize controller.
         */
        onInit: function () {
            /**
             * Store the link to "this"
             * @type {andrei.kuzmich.controller.OrdersOverview}
             */
            var that = this;

            /**
             * Get the component
             * @type {sap.ui.core.Component|*}
             */
            var oComponent = this.getOwnerComponent();

            /**
             * Get model odata from oComponent and set to view
             * @type {sap.ui.model.Model}
             */
            var oModel = oComponent.getModel("odata");
            var oViewModel = new JSONModel({
                commonCount: {
                    All: 0,
                    Pending: 0,
                    Accepted: 0
                }
            });
            this.oViewModel = oViewModel;


            /**
             *  Set models to the view
             */
            this.viewModel = this.getView().setModel(oViewModel, "viewModel");
            this.getView().setModel(oModel, "odata");

        },

        /**
         * Call Read method for each status
         * if status call 'All', call read method without filter
         * @param sStatus {string} status in oViewModel>/commonCount
         */
        getReadCount: function(sStatus) {
            var that = this;
            if(sStatus === 'All'){
                this.getView().getModel("odata").read("/Orders/$count", {
                    success: function(nCount){
                        that.oViewModel.setProperty(`/commonCount/All`, nCount);
                    }
                });
            } else {
                this.getView().getModel("odata").read("/Orders/$count", {
                    success: function(nCount){
                        that.oViewModel.setProperty(`/commonCount/${sStatus}`, nCount);
                    },
                    filters: [
                        new Filter(`summary/status`, FilterOperator.EQ, `'${sStatus}'`)
                    ]
                });
            }
        },

        /**
         * Open product page button press event handler.
         * @param oEvent event object.
         */
        onOpenDetailPagePress: function (oEvent) {
            /**
             * Get the source control of event object (the one that was fired event)
             */
            var oSource = oEvent.getSource();

            /**
             * Get the binding context of a button (it's a part of the table line, so it inherits the context of it)
             */
            var oCtx = oSource.getBindingContext("odata");

            /**
             * Get the component
             * @type {sap.ui.core.Component|*}
             */
            var oComponent = this.getOwnerComponent();

            /**
             * Get the router object and call "navTo" method to redirect user to the 2nd page, passing the mandatory
             * parameter "OrderID"
             */
            oComponent.getRouter().navTo("OrderDetails", {
                OrderID: oCtx.getObject("id")
            });
        },


        /**
         * Controller's "onAfterRendering" lifecycle method.
         * Call when render/rerender view
         */
        onAfterRendering: function(){
            var that = this;
            /**
             * Get Table by id from view
             */
            var oProductsTable = this.byId("OrdersTable");

            /**
             * Get binding items from view
             */
            var oItemsBinding = oProductsTable.getBinding("items");

            /**
             * Attach "dataReceived" event (to grab the Order count and show it on a screen)
             */
            oItemsBinding.attachDataReceived(function (oEvent) {
                // var mData = oEvent.getParameter("data");

                /**
                 * Call odata read method as many times as a status in model oViewModel
                 */
                Object.keys(this.oViewModel.getData().commonCount).forEach(function (value) {
                    that.getReadCount(value);
                });

            }, this);

        },

        /**
         * Filter orders table by status, when fire select
         * Get item key and filter by status
         * @param oEvent event object.
         */
        onFilterBarPress: function(oEvent) {
            /**
             * Object with filter parameters
             * @type {{Pending: *, Accepted: *, All: Array}} new Filter with parameters
             */
            var oFilter = {
                "Pending": new Filter("summary/status", FilterOperator.EQ, "'Pending'"),
                "Accepted": new Filter("summary/status", FilterOperator.EQ, "'Accepted'"),
                "All": []
            };

            /**
             * Find order table by id from view
             */
            var oProductsTable = this.byId("OrdersTable");
            /**
             * Get binding items
             */
            var oBindingItems = oProductsTable.getBinding("items");
            /**
             * Get parameter key from clicked item
             */
            var skey = oEvent.getParameter("key");
            /**
             * Filter order table item with filter parameter from 'oFilter'
             */
            oBindingItems.filter(oFilter[skey]);
        },

        /**
         * "Open dialog" button press event handler.
         */
        onAddOrderPress: function () {
            var oView = this.getView();
            var oODataModel = oView.getModel("odata");

            if(!this.oDialog) {
                this.oDialog = sap.ui.xmlfragment(oView.getId(), "andrei.kuzmich.view.fragments.AddOrderDialog", this);
                oView.addDependent(this.oDialog);
            }

            var oEntryCtx = oODataModel.createEntry("/Orders", {
                properties: {
                    summary:{
                        totalPrice: "0"
                    },
                    shipTo: {},
                    customerInfo: {}
                }
            });

            /**
             * Set context to the dialog
             */
            this.oDialog.setBindingContext(oEntryCtx);

            /**
             * Set default model to allow relative binding without a need to specify model's name
             */
            this.oDialog.setModel(oODataModel);

            /**
             * Open the dialog
             */
            this.oDialog.open();
        },

        /**
         * "Cancel" button press event handler (in the dialog).
         */
        onDialogCancelPress: function () {
            var oODataModel = this.getView().getModel("odata");

            var oCtx = this.oDialog.getBindingContext();

            /**
             *  Delete the entry from requests queue
             */
            oODataModel.deleteCreatedEntry(oCtx);

            /**
             * Close the dialog
             */
            this.oDialog.close();
        },

        /**
         * Validate helper method.
         * Check input value and if not correct set Error
         * @param oInput inout validate
         * @returns {boolean}
         * @private
         */
        _validateInput: function(oInput) {
            // debugger
            var oBinding = oInput.getBinding("value");
            var sValueState = "None";
            var bValidationError = false;

            try {
                oBinding.getType().validateValue(oInput.getValue());
            } catch (oException) {
                sValueState = "Error";
                bValidationError = true;
            }

            oInput.setValueState(sValueState);

            return bValidationError;
        },

        /**
         * Dialog "Create" button press event handler.
         */
        onDialogCreatePress: function () {
            var oODataModel = this.getView().getModel("odata");
debugger
            var that = this;
            var oView = this.getView();

            var aInputs = [
                oView.byId("CustomerInput"),
                oView.byId("StatusInput"),
                oView.byId("CurrencyInput"),
            ];
            var bValidationError = false;

            jQuery.each(aInputs, function (i, oInput) {
                bValidationError = that._validateInput(oInput) || bValidationError;
            });

            if (!bValidationError) {
                /**
                 * Call the method to "release" the changes from queue
                 */
                oODataModel.submitChanges();

                MessageToast.show("Order was successfully added!");

                /**
                 * Close the dialog
                 */
                this.oDialog.close();
            } else {
                MessageBox.alert("A validation error has occured. Complete your input first");
            }
        },

        /**
         * onChange update valueState of input
         */
        onChange: function(oEvent) {
            var oInput = oEvent.getSource();
            this._validateInput(oInput);
        },

        /**
         * "Delete" Order button press event handler.
         * @param oEvent {sap.ui.base.Event} event object
         */
        handleDelete: function (oEvent) {
            var that = this;
            var oCtx = oEvent.getParameter("listItem").getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/Orders", oCtx.getObject());

            MessageBox.confirm("Do you want to delete the order ?", {
                title : 'Confirm order removal!',
                onClose : function(sButton) {
                    if (sButton === MessageBox.Action.OK) {
                        that.removeOrderByKey(oODataModel, sKey)
                    } else if (sButton === MessageBox.Action.CANCEL) {
                        MessageToast.show("Deletion was canceled by user!");
                    }
                }
            });
        },

        /**
         * Execute "delete" request of the entity, specified in a key
         * @param oODataModel {oModel} model of remove item
         * @param sKey {string} remove item key
         */
        removeOrderByKey: function (oODataModel, sKey) {
            oODataModel.remove(sKey, {
                success: function () {
                    MessageToast.show("Order was successfully removed!")
                },
                error: function () {
                    MessageBox.error("Error while removing order!");
                }
            });
        },

	});
});

