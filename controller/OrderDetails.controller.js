sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "../Util/Util",
    "sap/m/MessageToast",
    "sap/m/MessageBox",
    "sap/ui/model/Sorter"
], function (Controller,JSONModel, Util, MessageToast, MessageBox, Sorter) {
    "use strict";

    /**
     * Constant for sorting asc decs and none
     * @type {string}
     */
    var SORT_NONE	= "";
    var SORT_ASC	= "ASC";
    var SORT_DESC	= "DESC";

    return Controller.extend("andrei.kuzmich.controller.OrderDetails", {
        /**
         *  Controller's "init" lifecycle method. Initialize controller.
         */
        onInit: function () {
            /**
             * Store the link to "this"
             * @type {andrei.kuzmich.controller.OrderDetails}
             */
            var that = this;

            /**
             * Get the component
             * @type {sap.ui.core.Component|*}
             */
            var oComponent = this.getOwnerComponent();

            /**
             * Create and set appView Model to view
             */
            this.oAppViewModel = new JSONModel({
                editShipping: false,
                editCustomer: false,
                sortType: SORT_NONE
            });
            this.getView().setModel(this.oAppViewModel, "appView");


            /**
             * Get the router object
             */
            var oRouter = oComponent.getRouter();

            /**
             * Set odata Model to view
             * @type {sap.ui.model.Model}
             */
            var oModel = oComponent.getModel("odata");
            this.getView().setModel(oModel, "odata");

            /**
             * Get the route object from router attach event handler, that will be called once the URL will match
             * The pattern of a router
             */
            oRouter.getRoute("OrderDetails").attachPatternMatched(this.onPatternMatched, this);
        },

        /**
         * Edit button press event handler on shipping
         */
        onEditPressShipping: function (oEvent) {
            this.oAppViewModel.setProperty("/editShipping", true);
        },

        /**
         * Edit button press event handler on customer
         */
        onEditPressCustomer: function (oEvent) {
            this.oAppViewModel.setProperty("/editCustomer", true);
        },

        /**
         * Save button press event handler on shipping
         */
        onSavePressShipping: function () {
            this.oAppViewModel.setProperty("/editShipping", false);

            var oODataModel = this.getView().getModel("odata");

            // call the method to release the request queue
            oODataModel.submitChanges();
        },

        /**
         * Save button press event handler on customer
         */
        onSavePressCustomer: function () {
            this.oAppViewModel.setProperty("/editCustomer", false);

            var oODataModel = this.getView().getModel("odata");

            /**
             * call the method to release the request queue
             */
            oODataModel.submitChanges();
        },

        /**
         * Cancel button press event handler on shipping
         */
        onCancelPressShipping: function () {
            this.oAppViewModel.setProperty("/editShipping", false);

            var oODataModel = this.getView().getModel("odata");

            /**
             * call the method to reset the request queue
             */
            oODataModel.resetChanges();
        },

        /**
         * Cancel button press event handler on customer
         */
        onCancelPressCustomer: function () {
            this.oAppViewModel.setProperty("/editCustomer", false);

            var oODataModel = this.getView().getModel("odata");

            /**
             * call the method to reset the request queue
             */
            oODataModel.resetChanges();
        },

        /**
         * "Orders Detail" route pattern matched event handler.
         * @param {sap.ui.base.Event} oEvent event object.
         */
        onPatternMatched: function (oEvent) {
            /**
             * Store the link to "this"
             * @type {andrei.kuzmich.controller.OrderDetails}
             */
            var that = this;

            /**
             * Get the route arguments from the event parameters
             * @type {any}
             */
            var mRouteArguments = oEvent.getParameter("arguments");

            /**
             * Get the "OrderID" parameter from arguments
             */
            var sOrderID = mRouteArguments.OrderID;

            /**
             * Get the ODataModel instance form the view (as the model was instantiated and set up in the Component,
             * the view has automatically access for it)
             */
            var oODataModel = this.getView().getModel("odata");

            /**
             * Wait until the metadata has been loaded. "metadataLoaded" method returns a promise
             */
            oODataModel.metadataLoaded().then(function () {
                /**
                 * Create an existent entity key, in order to be able to bind the view to it
                 * this method takes the name of EntitySet (collection) and map of key parameters
                 */
                var sKey = oODataModel.createKey("/Orders", {id: sOrderID});
                that.orderKey = sKey;
                /**
                 * Bind the whole view to supplier key (ODataModel will automatically request the data)
                 */
                that.getView().bindObject({
                    path: sKey,
                    model: "odata"
                });
            })
        },

        /**
         * Open product page button press event handler.
         * @param oEvent event object.
         */
        onOpenProductPagePress: function (oEvent) {
            /**
             * Get the source control of event object (the one that was fired event)
             */
            var oSource = oEvent.getSource();

            /**
             * Get the binding context of a button (it's a part of the table line, so it inherits the context of it)
             */
            var oCtx = oSource.getBindingContext("odata");

            /**
             * Get the component
             * @type {sap.ui.core.Component|*}
             */
            var oComponent = this.getOwnerComponent();

            /**
             * Get the router object and call "navTo" method to redirect user to the 2nd page, passing the mandatory
             * parameter "ProductDetailsID"
             */
            oComponent.getRouter().navTo("ProductDetails", {
                ProductDetailsID: oCtx.getObject("id")
            });
        },

        /**
         * Open dialog with add product form
         * @param oEvent event object.
         */
        onAddProductPress: function(oEvent) {
            var CurrentOrderId = oEvent.getSource().getBindingContext("odata").getObject("id");

            var oView = this.getView();
            var oODataModel = oView.getModel("odata");

            if(!this.oDialog) {
                this.oDialog = sap.ui.xmlfragment(oView.getId(), "andrei.kuzmich.view.fragments.AddProductDialog", this);
                oView.addDependent(this.oDialog);
            }

            var oEntryCtx = oODataModel.createEntry("/OrderProducts", {
                properties: {
                    orderId: CurrentOrderId,
                }
            });

            /**
             * Set context to the dialog
             */
            this.oDialog.setBindingContext(oEntryCtx);

            /**
             * Set default model to allow relative binding without a need to specify model's name
             */
            this.oDialog.setModel(oODataModel);

            /**
             * Open the dialog
             */
            this.oDialog.open();
        },

        /**
         * "Cancel" button press event handler (in the dialog).
         */
        onDialogCancelPress: function () {
            var oODataModel = this.getView().getModel("odata");

            var oCtx = this.oDialog.getBindingContext();

            /**
             *  Delete the entry from requests queue
             */
            oODataModel.deleteCreatedEntry(oCtx);

            /**
             * Close the dialog
             */
            this.oDialog.close();
        },

        /**
         * Dialog "Create" button press event handler (in the dialog).
         */
        onDialogCreatePress: function (oEvent) {
            var oODataModel = this.getView().getModel("odata");


            var that = this;
            var oView = this.getView();

            var aInputs = [
                oView.byId("ProductName"),
                oView.byId("ProductPrice"),
                oView.byId("ProductQty"),
            ];
            var bValidationError = false;

            jQuery.each(aInputs, function (i, oInput) {
                bValidationError = that._validateInput(oInput) || bValidationError;
            });

            if (!bValidationError) {
                this.updateTotalPrice(oEvent);

                /**
                 * Call the method to "release" the changes from queue
                 */
                oODataModel.submitChanges();

                MessageToast.show("Order was successfully added!");

                /**
                 * Close the dialog
                 */
                this.oDialog.close();
            } else {
                MessageBox.alert("A validation error has occured. Complete your input first");
            }
        },

        /**
         * Update order Total price when create product
         * @param oEvent
         */
        updateTotalPrice: function(oEvent) {
            var oODataModel = this.getView().getModel("odata");

            /**
             * Set total price for one product
             */
            var oFieldsValue = oEvent.getSource().getBindingContext().getObject();
            var nProductTotalPrice = oFieldsValue.price * oFieldsValue.quantity;
            var currentId = oEvent.getSource().getBindingContext().getPath();

            oODataModel.setProperty(currentId + "/totalPrice", nProductTotalPrice);
            // Order totalPrice in summary/totalPrice
            var nSummaryTotalPrice = oODataModel.getProperty(this.orderKey + "/summary/totalPrice");
            var nUpdateSummaryTotalPrice = Number(nSummaryTotalPrice) + nProductTotalPrice;
            oODataModel.setProperty(this.orderKey + '/summary/totalPrice', String(nUpdateSummaryTotalPrice));
        },

        /**
         * "Delete" Order button press event handler.
         * @param oEvent {sap.ui.base.Event} event object
         */
        handleDelete: function (oEvent) {
            var that = this;
            var oCtx = oEvent.getParameter("listItem").getBindingContext("odata");
            var oODataModel = oCtx.getModel();
            var sKey = oODataModel.createKey("/OrderProducts", oCtx.getObject());

            MessageBox.confirm("Do you want to delete the product ?", {
                title : 'Confirm product removal!',
                onClose : function(sButton) {
                    if (sButton === MessageBox.Action.OK) {
                        that.removeProductByKey(oODataModel, sKey)
                    } else if (sButton === MessageBox.Action.CANCEL) {
                        MessageToast.show("Deletion was canceled by user!");
                    }
                }
            });
        },

        /**
         * Execute "delete" request of the entity, specified in a key
         * @param oODataModel {oModel} model of remove item
         * @param sKey {string} remove item key
         */
        removeProductByKey: function (oODataModel, sKey) {
            oODataModel.remove(sKey, {
                success: function () {
                    MessageToast.show("Product was successfully removed!")
                },
                error: function () {
                    MessageBox.error("Error while removing order!");
                }
            });
        },

        /**
         * Formatter for the icon used in a sort trigger button.
         *
         * @param {string} sSortType sorting type.
         *
         * @returns {string} icon name.
         */
        sortTypeFormatter: function (sSortType) {
            switch (sSortType) {
                case SORT_NONE: {
                    return "sort";
                }
                case SORT_ASC: {
                    return "sort-ascending";
                }
                case SORT_DESC: {
                    return "sort-descending";
                }
                default: {
                    return "sort";
                }
            }
        },

        /**
         * "Sort" button press event handler.
         */
        onSortButtonPress: function () {
            var sSortType = this.oAppViewModel.getProperty("/sortType");
            switch (sSortType) {
                case SORT_NONE: {
                    sSortType = SORT_ASC;
                    break;
                }

                case SORT_ASC: {
                    sSortType = SORT_DESC;
                    break;
                }

                case SORT_DESC: {
                    sSortType = SORT_ASC;
                    break;
                }
            }
            /**
             * Update the models' property with new sorting type
             */
            this.oAppViewModel.setProperty("/sortType", sSortType);

            /**
             * get products table control
             */
            var oProductsTable = this.byId("ProductsTable");

            /**
             * get the "items" binding object from the products table
             */
            var oItemsBinding = oProductsTable.getBinding("items");

            /**
             * create sorter object
             * @type {boolean}
             */
            var bSortDesc = sSortType === SORT_DESC;
            var oSorter = new Sorter("name", bSortDesc);

            oItemsBinding.sort(oSorter);
        },

        /**
         * Validate helper method.
         * Check input value and if not correct set Error
         * @param oInput inout validate
         * @returns {boolean}
         * @private
         */
        _validateInput: function(oInput) {
            var oBinding = oInput.getBinding("value");
            var sValueState = "None";
            var bValidationError = false;

            try {
                oBinding.getType().validateValue(oInput.getValue());
            } catch (oException) {
                sValueState = "Error";
                bValidationError = true;
            }

            oInput.setValueState(sValueState);

            return bValidationError;
        },


        /**
         * Makes the transition to the previous page
         */
        goBack: function () {
            Util.onNavBack.bind(this)();
        }


    });
});

