sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/model/json/JSONModel",
    "sap/m/MessageToast",
], function (Controller,JSONModel, MessageToast) {
    "use strict";

    return Controller.extend("andrei.kuzmich.controller.ProductDetails", {
        /**
         * Controller's "init" lifecycle method. Initialize controller.
         */
        onInit: function () {
            /**
             * Store the link to "this"
             * @type {andrei.kuzmich.controller.ProductDetails}
             */
            var that = this;

            /**
             * Get the component
             * @type {sap.ui.core.Component|*}
             */
            var oComponent = this.getOwnerComponent();

            var oModel = oComponent.getModel("odata");
            this.getView().setModel(oModel, "odata");

            /**
             * Get the router object
             */
            var oRouter = oComponent.getRouter();

            /**
             * Get the route object from router attach event handler, that will be called once the URL will match
             * The pattern of a router
             */
            oRouter.getRoute("ProductDetails").attachPatternMatched(this.onPatternMatched, this);

        },
        /**
         * "Product Detail" route pattern matched event handler.
         * @param oEvent event object.
         */
        onPatternMatched: function (oEvent) {
            /**
             * Store the link to "this"
             * @type {andrei.kuzmich.controller.ProductDetails}
             */
            var that = this;

            /**
             * Get the route arguments from the event parameters
             */
            var mRouteArguments = oEvent.getParameter("arguments");

            /**
             * Get the "ProductDetailsID" parameter from arguments
             */
            var sOrderID = mRouteArguments.ProductDetailsID;

            this.orderID = sOrderID;

            /**
             * Get the ODataModel instance form the view (as the model was instantiated and set up in the Component,
             * the view has automatically access for it)
             */
            var oODataModel = this.getView().getModel("odata");

            /**
             * Wait until the metadata has been loaded. "metadataLoaded" method returns a promise
             */
            oODataModel.metadataLoaded().then(function () {
                /**
                 * Create an existent entity key, in order to be able to bind the view to it
                 * This method takes the name of EntitySet (collection) and map of key parameters
                 */
                var sKey = oODataModel.createKey("/OrderProducts", {id: sOrderID});
                that.commentPageKey = sKey;
                /**
                 * Bind the whole view to supplier key (ODataModel will automatically request the data)
                 */
                that.getView().bindObject({
                    path: sKey,
                    model: "odata"
                });
            });
        },

        /**
         * Create Comment with rating and author name for product
         * @param oEvent
         */
        onPost: function(oEvent) {
            var oView = this.getView();
            var oODataModel = oView.getModel("odata");
            var authorName = this.getView().byId("author_name").getValue();
            var rating = this.getView().byId("RI_default").getValue();


            var oEntryCtx = oODataModel.createEntry("/ProductComments(" + this.orderID + ")/comments", {
                properties: {
                    "comment": oEvent.getParameter("value"),
                    "author": authorName,
                    "createdDate": "/Date(1543923989932)/",
                    "rating": rating,
                    "productId": Number(this.orderID),
                }
            });
            oODataModel.submitChanges();
        },

        /**
         * Makes the transition to the previous page
         */
        onNavBack: function () {
            history.go(-1);
        }
    });
});

